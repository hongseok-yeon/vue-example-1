import Vue from 'vue'
import Router from 'vue-router'
import Step from './views/Step'
import Step1 from './views/Step1'
import Step2 from './views/Step2'
import Step3 from './views/Step3'
import Step4 from './views/Step4'
import Step5 from './views/Step5'
import Step6 from './views/Step6'
import Step7 from './views/Step7'
import Step8 from './views/Step8'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/step/1'
    },
    {
      path: '/step',
      name: 'step',
      component: Step,
      children: [
        {
          path: '1',
          name: 'step1',
          component: Step1
        },
        {
          path: '2',
          name: 'step2',
          component: Step2
        },
        {
          path: '3',
          name: 'step3',
          component: Step3
        },
        {
          path: '4',
          name: 'step4',
          component: Step4
        },
        {
          path: '5',
          name: 'step5',
          component: Step5
        },
        {
          path: '6',
          name: 'step6',
          component: Step6
        },
        {
          path: '7',
          name: 'step7',
          component: Step7
        },
        {
          path: '8',
          name: 'step8',
          component: Step8
        }
      ]
    }
  ]
})

console.log(router)

export default router
